package Day3.Pola;

import Day3.Pola.Angka;

public class MesinPola {
    public void Cetak(int n) {
        Angka ka = new Angka();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n * (n - 1) + 1; j++) {
                if (i == 0 || j == 0 || j == (n - 1) || i == (n - 1) || (j % (n - 1) == 0)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
