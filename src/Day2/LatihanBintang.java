package Day2;

import java.util.Scanner;

public class LatihanBintang {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Nilai N : ");
        int n = s.nextInt();
        System.out.print("Nilai N adalah : ");
        System.out.println(n);
        System.out.println();
        for(int i=0; i<n;i++){
            System.out.println("*");
        }
    }
}
