package Day2;

import java.util.Scanner;

public class Quiz1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Nilai : ");
        int n = s.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 0 ){
                    System.out.print("*");
                } else if (i % 4 == 1 && j == n-1 ){
                    System.out.print("-");
                } else if (i % 4 == 3 && j == 0){
                    System.out.print("-");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
